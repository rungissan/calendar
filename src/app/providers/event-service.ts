import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { map } from 'rxjs/operators';
import { Event } from '@models/event.model';

@Injectable()
export class EventService {
  private API_PATH = 'https://www.googleapis.com/events/v1/day';
  constructor(private http: HttpClient) {}

  getEvents(): Observable<Event[]> {
    return this.http.get<Event[]>('assets/data/scheduleevents.json');
  }
  searchEvents(queryTitle: string): Observable<Event[]> {
    return this.http
      .get<{ items: Event[] }>(`${this.API_PATH}?q=${queryTitle}`)
      .pipe(map(events => events.items || []));
  }

  retrieveEvent(volumeId: string): Observable<Event> {
    return this.http.get<Event>(`${this.API_PATH}/${volumeId}`);
  }
}
