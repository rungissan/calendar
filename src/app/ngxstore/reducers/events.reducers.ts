import { createSelector } from '@ngrx/store';
import { createEntityAdapter, EntityAdapter } from '@ngrx/entity';
import { Event } from '@models/event.model';
import * as fromActions from '@actions/events.actions';
import { EventsState } from './app.states';

const eventAdapter = createEntityAdapter<Event>();

export const initialState: EventsState = eventAdapter.getInitialState({
  selectedEventId: null
});

export function reducer(
  state = initialState,
  action: fromActions.All
): EventsState {
  switch (action.type) {
    case fromActions.ADD_ONE:
      return eventAdapter.addOne(action.event, state);
    case fromActions.UPDATE_ONE:
      return eventAdapter.updateOne(
        {
          id: action.id,
          changes: action.changes
        },
        state
      );
    case fromActions.DELETE_ONE:
      return eventAdapter.removeOne(action.id, state);
    case fromActions.GET_ALL:
      return eventAdapter.addAll(action.events, state);
    default:
      return state;
  }
}

export const {
  selectIds,
  selectEntities,
  selectAll,
  selectTotal
} = eventAdapter.getSelectors();
