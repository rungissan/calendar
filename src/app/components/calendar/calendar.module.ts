import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CalendarRoutingModule } from './calendar-routing.module';
import { CalendarComponent } from './calendar.component';
import { ScheduleModule } from 'primeng/schedule';
import { EventService } from '@providers/event-service';

@NgModule({
  imports: [
    CommonModule,
    ScheduleModule,
    CalendarRoutingModule
  ],
  declarations: [CalendarComponent],
  providers:[EventService],
  exports: [CalendarComponent],
})
export class CalendarModule { }
