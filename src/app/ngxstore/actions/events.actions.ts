import { Action } from '@ngrx/store';
import { Event } from '@models/event.model';

export const ADD_ONE = '[Event] Add One';
export const UPDATE_ONE = '[Event] Update One';
export const DELETE_ONE = '[Event] Delete One';
export const GET_ALL = '[Event] Get All';

export class AddOneEvent implements Action {
  readonly type = ADD_ONE;
  constructor(public event: Event) {}
}

export class UpdateOneEvent implements Action {
  readonly type = UPDATE_ONE;
  constructor(public id: string, public changes: Partial<Event>) {}
}

export class DeleteOneEvent implements Action {
  readonly type = DELETE_ONE;
  constructor(public id: string) {}
}

export class GetAllEvents implements Action {
  readonly type = GET_ALL;
  constructor(public events: Event[]) {}
}

export type All = AddOneEvent | UpdateOneEvent | DeleteOneEvent | GetAllEvents;
