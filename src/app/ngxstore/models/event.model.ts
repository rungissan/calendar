export interface Event {
  uid: string;
  title: string;
  start: string;
  end: string;
  className: string[];
}
