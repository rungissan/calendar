import { Component, OnInit } from '@angular/core';

import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl
} from '@angular/forms';
import { Store } from '@ngrx/store';
import { AllBarsState } from '@reducers/app.states';
import * as AllBarsReducer from '@reducers/allbars.reducers';
import * as fromAllBarsActions from '@actions/allbars.actions';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-truck-event-dialog',
  templateUrl: './truck-event-dialog.component.html',
  styleUrls: ['./truck-event-dialog.component.scss']
})
export class TruckEventDialogComponent implements OnInit {
  eventForm: FormGroup;
  allBars$: Observable<AllBarsState>;

  constructor(
    private fb: FormBuilder,
    private allbarstore: Store<AllBarsState>
  ) {
    this.allBars$ = allbarstore.select(AllBarsReducer.getAllBarsStates);
  }
  get email() {
    return this.eventForm.get('email').value;
  }
  get startdate() {
    return this.eventForm.get('startdate').value;
  }
  get password() {
    return this.eventForm.get('password').value;
  }

  ngOnInit() {
    this.eventForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      startdate: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }
  insertEvent(): void {
    this.allbarstore.dispatch(new fromAllBarsActions.NoDisplayEventAction());
  }
  updateEvent(): void {
    this.allbarstore.dispatch(new fromAllBarsActions.NoDisplayEventAction());
  }
  cancel(): void {
    this.allbarstore.dispatch(new fromAllBarsActions.NoDisplayEventAction());
  }
}
