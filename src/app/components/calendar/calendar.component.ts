import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';
import { Store, select } from '@ngrx/store';
import * as fromEventsActions from '@actions/events.actions';
import * as fromEvents from '../../ngxstore/reducers/events.reducers';
import { EventsState } from '@reducers/app.states';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent implements OnInit {
  businessHours: any;
  header: any;
  eventList$: Observable<any[]>;

  constructor(private store: Store<EventsState>) {
    this.eventList$ = store.select(fromEvents.selectAll);
  }

  ngOnInit() {
    this.header = {
      left: 'prev,next today',
      center: 'title',
      right: 'agendaDay,listWeek,agendaWeek'
    };
    this.businessHours = {
      dow: [1, 2, 3, 4, 5],
      start: '10:00',
      end: '20:00'
    };
  }
  handleEventClick($event) {
    console.log('handleEventClick' + $event);
  }
  handleDayClick($event) {
    console.log($event);
  }
  handleEventRender = $event => {
    console.log('I render1');
    console.log($event);
  };
  drop($event) {
    alert('Dropped on ');
  }
  loadEvents($event) {
    console.log($event.view);
  }

  handleDayRender = (date, cell) => {
    console.log('I rendering day');
  };

  getDate() {
    return new Date();
  }
}
